<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T5Ab#x2^*1ui1QN)e#L^K5A9JIy$ 7 }9E4]q*r$u#3U}/(}$q{#5NGs]ZJU^}i_' );
define( 'SECURE_AUTH_KEY',  '^z8_DvI;.?Cn^0uujcVDavR<!GzKd?RRsmVL@x:qa?x2GqevGC)ti(Q L6W8/bBY' );
define( 'LOGGED_IN_KEY',    '1-EOO7;@%fOpJO>h^qoRypW|*<+(o3:99a=}8A=YU,Dmw|% X4V52Ru_Gvk$;4<R' );
define( 'NONCE_KEY',        'Tf6S`oxCZuV:%p}q)_c.G51mtE/W%Fcapd~H3@PJ^@3]f)zGvR|iS5qTKLiU%)U+' );
define( 'AUTH_SALT',        '=jr)lsRJ~;9k3Xky3o!jv1Mnja{NI+N$n.Q?v>hYnX59+?@6n3)>m9bkt (Qo7yn' );
define( 'SECURE_AUTH_SALT', 'sZZJXlnxDhoU0@w(`T]po7XN8{iN_RC*U@U.ZObClQ5W4r/F5j^uplrhbB<xQ}MO' );
define( 'LOGGED_IN_SALT',   'oTZID[4DRqXW.cIf&eo(62Bj%(R:0VO0M%yCF{IE(L>R0U[[6I*Q}}|A9 dZYZ-Q' );
define( 'NONCE_SALT',       '.T]_4o?|dkr`r1VQS{qb*/gapqe8&08,z-@TCMIeP`T(+?kW461F({o1F_7BeQ_T' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
