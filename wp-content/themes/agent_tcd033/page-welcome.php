<?php /* Template Name: Welcome Screen */
get_header();
$options = get_desing_plus_option();
?>

  <?php $slider_image1 = wp_get_attachment_image_src($options['slider_image1'], 'full'); ?>
  <?php $slider_image2 = wp_get_attachment_image_src($options['slider_image2'], 'full'); ?>
  <?php $slider_image3 = wp_get_attachment_image_src($options['slider_image3'], 'full'); ?>
  <div id="top" class="heightasviewport">
    <?php if($slider_image1){ if($options['slider_url1']){ echo '<a href="'.$options['slider_url1'].'">'; }; ?><div class="heightasviewport splash-image" style="background-image:url(<?php echo $slider_image1[0]; ?>)"></div><?php if($options['slider_url1']){ echo '</a>'; }}; ?>
    <?php if($slider_image2){ if($options['slider_url2']){ echo '<a href="'.$options['slider_url2'].'">'; }; ?><div class="heightasviewport splash-image" style="background-image:url(<?php echo $slider_image2[0]; ?>)"></div><?php if($options['slider_url2']){ echo '</a>'; }}; ?>
    <?php if($slider_image3){ if($options['slider_url3']){ echo '<a href="'.$options['slider_url3'].'">'; }; ?><div class="heightasviewport splash-image" style="background-image:url(<?php echo $slider_image3[0]; ?>)"></div><?php if($options['slider_url3']){ echo '</a>'; }}; ?>

    <div class="heightasviewport no-pointer-events" style="position:relative">
      <div class="verticalcenter container">
        <div class="row">
          <div id="agent-splash-text" class="col-xs-120 translated-right serif animated">
            <h2 class="agent-splash-text-h2 smaller-mobile-h2"><?php echo nl2br($options['top_content1_headline']); ?></h2>
          </div>
        </div>
      </div>
    </div>

    <?php if($options['top_content1_btnsize']!='0'): ?>
    <a href="#section-two" class="next-button animated serif" style="width:<?php if(is_mobile()){echo $options['top_content1_btnsize_mobile'];}else{echo $options['top_content1_btnsize'];}; ?>px; height:<?php if(is_mobile()){echo $options['top_content1_btnsize_mobile'];}else{echo $options['top_content1_btnsize'];}; ?>px;">
        <span><?php echo nl2br($options['top_content1_btnlabel']); ?></span>
    </a>
    <?php endif; ?>
  </div>

  <?php $top_main_image2 = wp_get_attachment_image_src($options['top_main_image2'], 'circle2'); ?>
  <div class="section" id="section-two">
    <div class="container">
      <div class="row">
        <div class="col-xs-120 text-center">
          <h3 class="section-two-h3 smaller-mobile-h2"><?php echo nl2br($options['top_content2_headline']); ?></h3>
          <div class="desc1"><?php echo nl2br($options['top_content2_bodytext']); ?></div>
          <div class="text-center">
            <img src="<?php echo $top_main_image2[0]; ?>" class="section-two_circle_img">
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php $top_main_image3 = wp_get_attachment_image_src($options['top_main_image3'], 'full'); ?>
  <div class="separator" data-parallax="scroll" data-speed="0.6" data-image-src="<?php echo $top_main_image3[0]; ?>">
    <div class="title">
      <div class="container">
        <div class="row">
          <div class="col-xs-120">
            <h4 class="liner"><p class="color_blue">事業内容</p></h4>
            <span class="lead romaji"><?php echo $options['top_content3_headline_sub']; ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php
    $top_content3_banner_image1 = wp_get_attachment_image_src($options['top_content3_banner_image1'], 'circle2');
    $top_content3_banner_image2 = wp_get_attachment_image_src($options['top_content3_banner_image2'], 'circle2');
    $top_content3_banner_image3 = wp_get_attachment_image_src($options['top_content3_banner_image3'], 'circle2');
  ?>
  <div class="section container">
    <div class="row">
      <div class="col-xs-120 no-padding">
        <div class="row text-left">
          <div class="text-center col-sm-40 mobile-mb-30" style="padding-left:30px;padding-right:30px;">
            <?php if($options['top_content3_banner_url1']){ ?><div class="circleimages"><a href="<?php echo $options['top_content3_banner_url1']; ?>"><img src="<?php echo $top_content3_banner_image1[0]; ?>"/></a></div><?php }else{ ?><div class="circleimages"><img src="<?php echo $top_content3_banner_image1[0]; ?>"/></div><?php }; ?>
            <h5 class="text-center section3-h5"><p class="color_blue"><?php echo nl2br($options['top_content3_banner_headline1']); ?></p></h5>
            <div class="text-justify desc2"><?php echo nl2br($options['top_content3_banner_body1']); ?></div>
            <?php if($options['top_content3_banner_url1']){ ?><a class="read-more romaji mobile-mt-0" href="<?php echo $options['top_content3_banner_url1']; ?>"><?php echo $options['top_content3_banner_btnlabel1']; ?></a><?php }; ?>
          </div>
          <div class="text-center col-sm-40 mobile-mb-30" style="padding-left:30px;padding-right:30px;">
            <?php if($options['top_content3_banner_url2']){ ?><div class="circleimages"><a href="<?php echo $options['top_content3_banner_url2']; ?>"><img src="<?php echo $top_content3_banner_image2[0]; ?>"/></a></div><?php }else{ ?><div class="circleimages"><img src="<?php echo $top_content3_banner_image2[0]; ?>" 
/></div><?php }; ?>
            <h5 class="text-center section3-h5"><p class="color_blue"><?php echo nl2br($options['top_content3_banner_headline2']); ?></p></h5>
            <div class="text-justify desc2"><?php echo nl2br($options['top_content3_banner_body2']); ?></div>
            <?php if($options['top_content3_banner_url2']){ ?><a class="read-more romaji mobile-mt-0" href="<?php echo $options['top_content3_banner_url2']; ?>"><?php echo $options['top_content3_banner_btnlabel2']; ?></a><?php }; ?>
          </div>
          <div class="text-center col-sm-40" style="padding-left:30px;padding-right:30px;">
            <?php if($options['top_content3_banner_url3']){ ?><div class="circleimages"><a href="<?php echo $options['top_content3_banner_url3']; ?>"><img src="<?php echo $top_content3_banner_image3[0]; ?>"/></a></div><?php }else{ ?><div class="circleimages"><img src="<?php echo $top_content3_banner_image3[0]; ?>"/></div><?php }; ?>
            <h5 class="text-center section3-h5"><p class="color_blue"><?php echo nl2br($options['top_content3_banner_headline3']); ?></p></h5>
            <div class="text-justify desc2"><?php echo nl2br($options['top_content3_banner_body3']); ?></div>
            <?php if($options['top_content3_banner_url3']){ ?><a class="read-more romaji mobile-mt-0" href="<?php echo $options['top_content3_banner_url3']; ?>"><?php echo $options['top_content3_banner_btnlabel3']; ?><span class="arrow_ico2"></span></a><?php }; ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php $top_main_image4 = wp_get_attachment_image_src($options['top_main_image4'], 'full'); ?>
  <div class="separator" data-parallax="scroll" data-speed="0.6" data-image-src="<?php echo $top_main_image4[0]; ?>">
    <div class="title">
      <div class="container">
        <div class="row">
          <div class="col-xs-120">
            <h4 class="liner"><?php echo $options['top_content4_headline']; ?></h4>
            <span class="lead romaji"><?php echo $options['top_content4_headline_sub']; ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="main-content">
    <div class="section container">
      <div class="row">
          <?php $the_query = new WP_Query("post_type=post&posts_per_page=6&orderby=date&order=DESC"); ?>
          <?php if ( $the_query->have_posts() ) : ?>

            <?php /* Start the Loop */ ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            <div class="col-sm-40 top_content4_post"><?php get_template_part( 'content', get_post_format() ); ?></div>
            <?php endwhile; ?>

          <?php endif; ?>
       <?php if($options['top_content4_indexurl']) { ?><a href="<?php echo $options['top_content4_indexurl']; ?>" class="archives_btn"><?php if($options['top_content4_indexlabel']){ echo $options['top_content4_indexlabel']; }else{ _e('Blog Index', 'tcd-w'); }; ?></a><?php }; ?>
    </div><!-- close .row -->
  </div><!-- close .container -->
</div><!-- close .main-content -->

  <?php $top_main_image5 = wp_get_attachment_image_src($options['top_main_image5'], 'full'); ?>
  <div class="separator" data-parallax="scroll" data-speed="0.6" data-image-src="<?php echo $top_main_image5[0]; ?>">
    <div class="title">
      <div class="container">
        <div class="row">
          <div class="col-xs-120">
            <h4 class="liner"><?php echo $options['top_content5_headline']; ?></h4>
            <span class="lead romaji"><?php echo $options['top_content5_headline_sub']; ?></span>
          </div>
        </div>
      </div>
    </div>
  </div>





<?php get_footer(); ?>